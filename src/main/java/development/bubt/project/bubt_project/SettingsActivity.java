package development.bubt.project.bubt_project;


import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.database.Cursor;
import android.os.Bundle;
import android.preference.Preference;
import android.support.v7.app.ActionBar;
import android.text.format.DateUtils;
import android.view.MenuItem;
import android.widget.Toast;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import development.bubt.project.s_assistant.R;

public class SettingsActivity extends AppCompatPreferenceActivity {
    Activity base = this;

    String timestamp;

    String futureDate;

    DateFormat df;
    Date now;

    String currentDate;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupActionBar();
        addPreferencesFromResource(R.xml.pref_general);

        String qu = "SELECT * FROM CLEAR_DATA";

        Cursor cursor = AppBase.handler.execQuery(qu);

        if(cursor == null || cursor.getCount() == 0) {
            //nothing
        } else {
            while(cursor.moveToNext()){
                timestamp = cursor.getString(0);

            }
            cursor.close();
        }

        //old time
        //currentDate = now
        //futureDate

        df = new SimpleDateFormat("yy-mm-dd HH:mm:ss");
        now = new Date();
        currentDate = df.format(now);

        Calendar c = Calendar.getInstance();

        try {
            c.setTime(df.parse(timestamp));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        c.add(Calendar.MONTH, 4);  // number of days to add
        futureDate = df.format(c.getTime());  // dt is now the new date



        Preference myPref = findPreference("clear_schedule");
        myPref.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            public boolean onPreferenceClick(Preference preference) {
                final AlertDialog.Builder alert = new AlertDialog.Builder(base);
                if(currentDate != futureDate) {
                    alert.setTitle("Couldn't delete");
                    alert.setMessage("Scheduling data can be deleted after the semester.");
                    alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            //Toast.makeText(getBaseContext(), futureDate, Toast.LENGTH_LONG ).show();
                            dialog.dismiss();
                        }
                    });
                } else {
                    alert.setTitle("Are you sure ?");
                    alert.setMessage("This will wipe your entire scheduling data");
                    alert.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            String qu = "DROP TABLE SCHEDULE";
                            if (AppBase.handler.execAction(qu)) {
                                Toast.makeText(getBaseContext(), "Done", Toast.LENGTH_LONG).show();
                                AppBase.handler.createTable();
                                dialog.dismiss();
                            }
                        }
                    });
                    alert.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            return;
                        }
                    });
                }
                alert.show();
                return true;
            }
        });
        myPref = findPreference("clear_attendance");
        myPref.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            public boolean onPreferenceClick(Preference preference) {
                final AlertDialog.Builder alert = new AlertDialog.Builder(base);

                if(currentDate != futureDate) {

                    alert.setTitle("Couldn't delete");
                    alert.setMessage("Attendance data can be deleted after the semester.");
                    alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            //Toast.makeText(getBaseContext(), futureDate, Toast.LENGTH_LONG ).show();
                            dialog.dismiss();
                        }
                    });

                } else {
                    alert.setTitle("Are you sure ?");
                    alert.setMessage("This will wipe your entire attendance data");
                    alert.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            String qu = "DROP TABLE ATTENDANCE";
                            if (AppBase.handler.execAction(qu)) {
                                Toast.makeText(getBaseContext(), "Done", Toast.LENGTH_LONG).show();
                                AppBase.handler.createTable();
                                dialog.dismiss();
                            }
                        }
                    });
                    alert.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            return;
                        }
                    });
                }
                alert.show();
                return true;
            }
        });

        myPref = findPreference("clear_notes");
        myPref.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            public boolean onPreferenceClick(Preference preference) {
                final AlertDialog.Builder alert = new AlertDialog.Builder(base);

                if (currentDate != futureDate) {
                    alert.setTitle("Couldn't delete");
                    alert.setMessage("Courses data can be deleted after the semester.");
                    alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            //Toast.makeText(getBaseContext(), futureDate, Toast.LENGTH_LONG ).show();
                            dialog.dismiss();
                        }
                    });

                } else {

                    alert.setTitle("Are you sure ?");
                    alert.setMessage("This will wipe your entire Notes data");
                    alert.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            String qu = "DROP TABLE NOTES";
                            if (AppBase.handler.execAction(qu)) {
                                Toast.makeText(getBaseContext(), "Done", Toast.LENGTH_LONG).show();
                                AppBase.handler.createTable();
                                dialog.dismiss();
                            }
                        }
                    });
                    alert.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            return;
                        }
                    });
                }
                alert.show();
                return true;
            }
        });

        myPref = findPreference("clear_student");
        myPref.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            public boolean onPreferenceClick(Preference preference) {
                final AlertDialog.Builder alert = new AlertDialog.Builder(base);

                if(currentDate != futureDate) {
                    alert.setTitle("Couldn't delete");
                    alert.setMessage("Student data can be deleted after the semester.");
                    alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            //Toast.makeText(getBaseContext(), futureDate, Toast.LENGTH_LONG ).show();
                            dialog.dismiss();
                        }
                    });
                } else {
                    alert.setTitle("Are you sure ?");
                    alert.setMessage("This will wipe your entire Student data");
                    alert.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            String qu = "DROP TABLE STUDENT";
                            if (AppBase.handler.execAction(qu)) {
                                Toast.makeText(getBaseContext(), "Done", Toast.LENGTH_LONG).show();
                                AppBase.handler.createTable();
                                dialog.dismiss();
                            }
                        }
                    });
                    alert.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            return;
                        }
                    });
                }
                alert.show();
                return true;
            }
        });
    }
    private void setupActionBar() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
