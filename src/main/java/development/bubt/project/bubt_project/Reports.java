package development.bubt.project.bubt_project;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.database.Cursor;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import com.opencsv.CSVWriter;

import java.io.File;
import java.io.FileWriter;
import java.text.DecimalFormat;
import java.util.ArrayList;

import development.bubt.project.s_assistant.R;

public class Reports extends AppCompatActivity {

    ArrayAdapter<String> adapterSpinner;
    Spinner spinner;
    ArrayList<String> names;
    ArrayList<String> registers;
    ArrayList<String> reportPercent;
    ArrayList<String> countAtt;
    Activity thisActivity = this;
    LinearLayout linearLayout;

    ListView listView;
    listReportAdapter adapter;
    DecimalFormat df = new DecimalFormat("#.00");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reports);
        linearLayout = (LinearLayout) findViewById(R.id.titlebar);
        linearLayout.setVisibility(View.GONE);

        listView = (ListView) findViewById(R.id.reportsView);
        names = new ArrayList<>();
        registers = new ArrayList<>();
        reportPercent = new ArrayList<>();
        countAtt = new ArrayList<>();

        Button btn = (Button) findViewById(R.id.loadButton);
        assert btn != null;

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadListView(v);
            }
        });

        spinner = (Spinner) findViewById(R.id.reportsSpinner);
        adapterSpinner = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, AppBase.divisions);
        assert spinner != null;
        spinner.setAdapter(adapterSpinner);


    }

    public void loadListView(View view) {
        float att = 0f;
        //Log.d("Attendance", "Started");
        names.clear();
        registers.clear();
        reportPercent.clear();
        countAtt.clear();
        String qu = "SELECT * FROM STUDENT WHERE cl = '" + spinner.getSelectedItem().toString() + "' " +
                "ORDER BY ROLL";

        Cursor cursor = AppBase.handler.execQuery(qu);

        if(cursor == null || cursor.getCount() == 0) {
            //action here
            //Log.d("Attendance", "In IF");
        } else {

            //export start
//            File exportDir = new File(Environment.getExternalStorageDirectory(), "");
//
//            if (!exportDir.exists())
//            {
//                exportDir.mkdirs();
//            }
//
//            File file = new File(exportDir, "csvname.csv");
//
//                try {
//                    file.createNewFile();
//                    CSVWriter csvWrite = new CSVWriter(new FileWriter(file));
//                    Cursor curCSV = AppBase.handler.execQuery(qu);
//                    //csvWrite.writeNext(curCSV.getColumnNames());
//                    while(curCSV.moveToNext())
//                    {
//                        //Which column you want to exprort
//                        String arrStr[] ={curCSV.getString(0),curCSV.getString(1), "90%"};
//                        csvWrite.writeNext(arrStr);
//
//                        Log.d("While", "Worked");
//                    }
//                    csvWrite.close();
//                    curCSV.close();
//
//                } catch (Exception sqlEx) {
//                    Log.e("Report Export", sqlEx.getMessage(), sqlEx);
//                }

            //export end

            linearLayout.setVisibility(View.VISIBLE);

            //Log.d("Attendance", "Else");
            int ctr = 0;
            cursor.moveToFirst();
            while (!cursor.isAfterLast())
            {
                //Log.d("REG", cursor.getString(2));
                String qc = "SELECT * FROM ATTENDANCE WHERE register = '"+ cursor.getString(2) +"'";
                String qa = "SELECT sum(isPresent) FROM ATTENDANCE WHERE register = '"+ cursor.getString(2) +"'";


                Cursor cur = AppBase.handler.execQuery(qc);

                Cursor total = AppBase.handler.execQuery(qa);

                total.moveToFirst();

                cur.moveToFirst();

                att = ((float) total.getFloat(0)/cur.getCount())*100;


                names.add(cursor.getString(0));
                registers.add(cursor.getString(2));
                countAtt.add(String.valueOf(cur.getCount()));
                reportPercent.add(df.format(att)+"%");
                cursor.moveToNext();
                total.moveToNext();
                cur.moveToNext();
                ctr++;

                att = 0;

            }
            if(ctr==0)
            {
                //Log.d("Attendance", "Out while");
                Toast.makeText(getBaseContext(),"No Students Found",Toast.LENGTH_LONG).show();
            }
            Log.d("Attendance", "Got " + ctr + " students");
        }

        adapter = new listReportAdapter(thisActivity,names,registers, reportPercent, countAtt);
        listView.setAdapter(adapter);


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.reports_menu, menu);
        return true;
    }

    public void exportReport(MenuItem item) {
        //Intent launchIntent  = new Intent(this,Edit_Student.class);
        //startActivity(launchIntent);

        spinner = (Spinner) findViewById(R.id.reportsSpinner);
        float att = 0f;
        //Log.d("Attendance", "Started");
        names.clear();
        registers.clear();
        reportPercent.clear();
        countAtt.clear();
        String qu = "SELECT * FROM STUDENT WHERE cl = '" + spinner.getSelectedItem().toString() + "' " +
                "ORDER BY ROLL";

        Cursor cursor = AppBase.handler.execQuery(qu);

        if(spinner == null) {
            Toast.makeText(this, "Select class",Toast.LENGTH_SHORT).show();
        } else {

            if(cursor == null || cursor.getCount() == 0) {
                //action here
                //Log.d("Attendance", "In IF");
                Toast.makeText(this, "No record found", Toast.LENGTH_SHORT).show();
            } else {
                File exportDir = new File(Environment.getExternalStorageDirectory(), "");

                if (!exportDir.exists())
                {
                    exportDir.mkdirs();
                }

                File file = new File(exportDir, "exportreport.csv");

                try{
                    file.createNewFile();
                    CSVWriter csvWrite = new CSVWriter(new FileWriter(file));
                    //Cursor curCSV = AppBase.handler.execQuery(qu);
                    //csvWrite.writeNext(curCSV.getColumnNames());
                    String columns[] = {"ID", "Name", "Attendance", "Percentage"}; //"first#second#third".split("#");
                    csvWrite.writeNext(columns);

                    while(cursor.moveToNext())
                    {
                        String qc = "SELECT * FROM ATTENDANCE WHERE register = '"+ cursor.getString(2) +"'";
                        String qa = "SELECT sum(isPresent) FROM ATTENDANCE WHERE register = '"+ cursor.getString(2) +"'";


                        Cursor cur = AppBase.handler.execQuery(qc);

                        Cursor total = AppBase.handler.execQuery(qa);

                        total.moveToFirst();

                        cur.moveToFirst();

                        att = ((float) total.getFloat(0)/cur.getCount())*100;


                        //Which column you want to exprort
                        String arrStr[] ={cursor.getString(2),cursor.getString(0), String.valueOf(cur.getCount()), df.format(att)+"%"};
                        csvWrite.writeNext(arrStr);

                        total.moveToNext();
                        cur.moveToNext();

                        //Log.d("While in TRY", "Worked");
                    }

                    csvWrite.close();
                    cursor.close();

                    att = 0;

                } catch (Exception sqlEx) {
                    Log.e("Report Export", sqlEx.getMessage(), sqlEx);
                }

                Toast.makeText(this, "Exporting CSV", Toast.LENGTH_SHORT).show();

            }

            ///Toast.makeText(this, spinner.getSelectedItem().toString(), Toast.LENGTH_SHORT).show();
        }




    }
}
