package development.bubt.project.bubt_project;

import android.app.Activity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.ArrayList;

import development.bubt.project.s_assistant.R;

public class listReportAdapter extends BaseAdapter {

    ArrayList<String> nameList;
    ArrayList<String> registers;
    ArrayList<String> per;
    ArrayList<String> countAtt;
    Activity activity;
    ArrayList<Boolean> attendanceList;

    public listReportAdapter(Activity activity,ArrayList<String> nameList,ArrayList<String> reg, ArrayList<String> per, ArrayList<String> count) {
        this.nameList = nameList;
        this.activity = activity;
        this.registers = reg;
        this.countAtt = count;
        this.per = per;
        attendanceList = new ArrayList<>();
        for(int i=0; i<nameList.size(); i++)
        {
            attendanceList.add(new Boolean(true));
        }
    }
    @Override
    public int getCount() {
        return nameList.size();
    }

    public float getTotal() { return nameList.size(); }

    @Override
    public Object getItem(int position) {
        return nameList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View v, ViewGroup parent) {
        if (v == null) {
            LayoutInflater vi = LayoutInflater.from(activity);
            v = vi.inflate(R.layout.list_reports, null);
        }
        final int pos = position;
        TextView textID = (TextView) v.findViewById(R.id.ID);
        TextView textView = (TextView) v.findViewById(R.id.attendanceName);
        TextView textPercent = (TextView) v.findViewById(R.id.reportpercentage);
        TextView textCount = (TextView) v.findViewById(R.id.attendanceCount);

        textID.setText(registers.get(position));
        textView.setText(nameList.get(position));
        textPercent.setText(per.get(position));
        textCount.setText(countAtt.get(position));

        return v;
    }
}
